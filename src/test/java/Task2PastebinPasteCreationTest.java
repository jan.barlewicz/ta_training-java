import com.epam.training.Jan_Barlewicz.WebDriver.Task2.Pages.PastebinHomepage;
import com.epam.training.Jan_Barlewicz.WebDriver.Task2.Pages.PastebinPasteCreationResult;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.junit.Test;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

import java.time.Duration;


public class Task2PastebinPasteCreationTest {
    private WebDriver webDriver;
    @Before
    public void setupBrowser(){
        EdgeOptions edgeOptions = new EdgeOptions();
        edgeOptions.addArguments("force-device-scale-factor=0.75");
        edgeOptions.addArguments("high-dpi-support=0.75");
        webDriver = new EdgeDriver(edgeOptions);
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }
    @Test
    public void MakeSimplePasteInPastebin() {
        String title = "how to gain dominance among developers";
        PastebinPasteCreationResult webPaste = new PastebinHomepage(webDriver)
                .openPage()
                .handlePrivacyPopup()
                .insertBashCode()
                .setHighlightToBash()
                .insertTile(title)
                .setExpirationTo10M()
                .submitPaste();

        Assert.assertEquals(webPaste.getPasteText(), "git config --global user.name  \"New Sheriff in Town\"\n" +
               "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
                "git push origin master --force");
        Assert.assertTrue(webPaste.getPageTile().contains(title));
        Assert.assertTrue(webPaste.isBashFormatting());
    }

    @After
    public void closeBrowser() {
        webDriver.close();
    }
}
