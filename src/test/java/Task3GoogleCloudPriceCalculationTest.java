import com.epam.training.Jan_Barlewicz.WebDriver.Task3.Pages.GoogleCloudCostEstimatePage;
import com.epam.training.Jan_Barlewicz.WebDriver.Task3.Pages.GoogleCloudHomepage;
import com.epam.training.Jan_Barlewicz.WebDriver.Task3.Pages.GoogleCloudPriceCalculatorPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

import java.time.Duration;

public class Task3GoogleCloudPriceCalculationTest {
    private WebDriver webDriver;
    @Before
    public void setupBrowser(){
        EdgeOptions edgeOptions = new EdgeOptions();
        edgeOptions.addArguments("force-device-scale-factor=0.75");
        edgeOptions.addArguments("high-dpi-support=0.75");
        webDriver = new EdgeDriver(edgeOptions);
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

    }

    @Test
    public void setCloudComponentsAndSendPriceEstimateOnMailTest(){

        GoogleCloudPriceCalculatorPage priceCalculatorPage = new GoogleCloudHomepage(webDriver)
                .performSearch()
                .selectResult()
                .goToEstimate()
                .setValues()
                .shareEstimate();
        GoogleCloudCostEstimatePage estimatePage = priceCalculatorPage.goToSummary();
        Assert.assertTrue(estimatePage.areValuesCorrect());


    }

    @After
    public void closeBrowser() {
        webDriver.close();
    }
}
