import com.epam.training.Jan_Barlewicz.Framework.Task1.Driver.DriverManager;
import com.epam.training.Jan_Barlewicz.Framework.Task1.Model.CloudSpecification;
import com.epam.training.Jan_Barlewicz.Framework.Task1.Pages.GoogleCloudCostEstimatePage;
import com.epam.training.Jan_Barlewicz.Framework.Task1.Pages.GoogleCloudHomepage;
import com.epam.training.Jan_Barlewicz.Framework.Task1.Pages.GoogleCloudPriceCalculatorPage;
import com.epam.training.Jan_Barlewicz.Framework.Task1.Pages.GoogleCloudSearchResults;
import com.epam.training.Jan_Barlewicz.Framework.Task1.Service.CloudSpecificationCreator;
import com.epam.training.Jan_Barlewicz.Framework.Task1.utils.TestListener;
import org.testng.annotations.AfterClass;
import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
@Listeners({TestListener.class})
public class Task1Frameworks {
    private WebDriver webDriver;
    @BeforeClass(alwaysRun = true)
    public void setupBrowser(){
        System.setProperty("browser", "edge");
        System.setProperty("environment", "defaultSpec");
        webDriver = DriverManager.getWebDriver();
    }

    @Test(groups = "fullTest")
    public void setCloudComponentsAndSendPriceEstimateOnMailTest(){
        CloudSpecification testSpecification = CloudSpecificationCreator.withDefaultSettings();

        GoogleCloudPriceCalculatorPage priceCalculatorPage = new GoogleCloudHomepage(webDriver)
                .performSearchForItems("Google Cloud Platform Pricing Calculator")
                .selectResult()
                .goToEstimate()
                .setValues(testSpecification)
                .shareEstimate(testSpecification);
        GoogleCloudCostEstimatePage estimatePage = priceCalculatorPage.goToSummary();
        Assert.assertTrue(estimatePage.areValuesCorrect(testSpecification));


    }

    @Test(groups = "searchTest")
    public void searchForTermTest(){
        GoogleCloudSearchResults priceCalculatorPage = new GoogleCloudHomepage(webDriver)
                .performSearchForItems("Google Cloud Platform Pricing Calculator");
        Assert.assertNotEquals(0,priceCalculatorPage.amountOfResults());


    }

    @AfterClass(alwaysRun = true)
    public void closeBrowser() {
        DriverManager.closeWebDriver();
    }
}
