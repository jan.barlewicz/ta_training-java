import com.epam.training.Jan_Barlewicz.WebDriver.Task1.Pages.PastebinHomepage;
import com.epam.training.Jan_Barlewicz.WebDriver.Task1.Pages.PastebinPasteCreationResolutionScreen;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

import java.time.Duration;

public class Task1PastebinPasteCreationWithSeleniumTest {
    private WebDriver webDriver;
    @Before
    public void setupBrowser(){
        EdgeOptions edgeOptions = new EdgeOptions();
        edgeOptions.addArguments("force-device-scale-factor=0.75");
        edgeOptions.addArguments("high-dpi-support=0.75");
        webDriver = new EdgeDriver(edgeOptions);
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @Test
    public void makeSimplePasteInPastebin() {
        PastebinPasteCreationResolutionScreen webPaste = new PastebinHomepage(webDriver).openPage()
                .insertCode("Hello from WebDriver")
                .insertTile("helloweb")
                .handlePrivacyPopup()
                .setExpirationTo10M()
                .submitPaste();
        Assert.assertEquals(webPaste.getPasteText(),"Hello from WebDriver");
        Assert.assertEquals(webPaste.getPasteTile(),"helloweb");
    }

    @After
    public void closeBrowser() {
        webDriver.close();
    }
}
