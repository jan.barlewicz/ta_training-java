package com.epam.training.Jan_Barlewicz.WebDriver.Task1.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PastebinPasteCreationResolutionScreen {
    private WebDriver webDriver;
    PastebinPasteCreationResolutionScreen(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public String getPasteText(){
        WebElement pasteText = webDriver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div[4]/div[2]/ol/li/div"));
        return pasteText.getText();
    }

    public String getPasteTile(){
        WebElement pasteText = webDriver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div[2]/div[3]/div[1]/h1"));
        return pasteText.getText();
    }
}
