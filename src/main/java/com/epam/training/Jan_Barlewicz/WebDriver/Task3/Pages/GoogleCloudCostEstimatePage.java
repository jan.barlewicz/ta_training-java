package com.epam.training.Jan_Barlewicz.WebDriver.Task3.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class GoogleCloudCostEstimatePage {
    private WebDriver webDriver;
    GoogleCloudCostEstimatePage(WebDriver webDriver){
        this.webDriver = webDriver;

    }

    public boolean areValuesCorrect(){
        boolean valuesTrue =  true;
        WebElement resultDiv = webDriver.findElement(By.xpath("//div[contains(@class,'wZCOB')]"));
        //List<WebElement> noOfInstances = resultDiv.findElements(By.className("EQCBxd"));
        List<WebElement> resultDivValues = resultDiv.findElements(By.className("Kfvdz"));
/*
        System.out.println(as.get(0).getText());
        for (WebElement a: as
             ) {
            System.out.println(a.getText());

        }
*/

        if(!resultDivValues.get(2).getText().equals("n1-standard-8, vCPUs: 8, RAM: 30 GB")){
            valuesTrue = false;
        }

        if(!resultDivValues.get(4).getText().equals("NVIDIA Tesla P100")){
            valuesTrue = false;
        }

        if(!resultDivValues.get(5).getText().equals("1")){
            valuesTrue = false;
        }

        if(!resultDivValues.get(6).getText().equals("2x375 GB")){
            valuesTrue = false;
        }

        if(!resultDivValues.get(9).getText().equals("4")){
            valuesTrue = false;
        }

        if(!resultDivValues.get(10).getText().equals("Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)")){
            valuesTrue = false;
        }

        if(!resultDivValues.get(11).getText().equals("Regular")){
            valuesTrue = false;
            System.out.println("error reg");
        }

        if(!resultDivValues.get(17).getText().equals("Netherlands (europe-west4)")){
            valuesTrue = false;
        }

        if(!resultDivValues.get(18).getText().equals("1 year")){
            valuesTrue = false;
        }

        return valuesTrue;
    }
}
