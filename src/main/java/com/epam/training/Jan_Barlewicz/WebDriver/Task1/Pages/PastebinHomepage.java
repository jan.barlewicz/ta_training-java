package com.epam.training.Jan_Barlewicz.WebDriver.Task1.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class PastebinHomepage {
    private final static String PASTEBIN_URL = "https://pastebin.com/";
    private WebDriver webDriver;

    private final By postformTextLocator = By.id("postform-text");
    private final By postformNameLocator = By.id("postform-name");
    private final By postformExpirationTimeFieldLocator = By.xpath("//div[contains(@class,'postform-expiration')]//div[contains(@class,'col-sm-9 field-wrapper')]");
    private final By postformExpirationTimeOptionListLocator = By.id("select2-postform-expiration-results");
    private final By postformExpirationTimeDesiredOptionLocator = By.xpath("//li[contains(text(), \"10 Minutes\")]");


    public PastebinHomepage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public PastebinHomepage openPage() {
        webDriver.get(PASTEBIN_URL);
        return this;
    }

    public PastebinHomepage insertCode(String codeString) {
        WebElement codeField = webDriver.findElement(postformTextLocator);
        codeField.sendKeys(codeString);
        return this;
    }

    public PastebinHomepage insertTile(String tileString) {
        WebElement codeField = webDriver.findElement(postformNameLocator);
        codeField.sendKeys(tileString);
        return this;
    }

    public PastebinHomepage handlePrivacyPopup() {
        new WebDriverWait(webDriver, Duration.ofSeconds(10))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[1]/div/div/div/div[2]/div/button[2]")));
        new WebDriverWait(webDriver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div/div/div/div[2]/div/button[2]")));
        WebElement privacyAccessButton = webDriver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[2]/div/button[2]"));
        privacyAccessButton.click();
        return this;
    }

    public PastebinHomepage setExpirationTo10M() {
        //WebElement selectFieldDiv = webDriver.findElement(By.xpath("//*[@id=\"w0\"]/div[5]/div[1]/div[4]/div"));
        WebElement selectFieldDiv = webDriver.findElement(postformExpirationTimeFieldLocator);
        selectFieldDiv.click();
        WebElement selectedOptionUl = webDriver.findElement(postformExpirationTimeOptionListLocator);
        selectedOptionUl.findElement(postformExpirationTimeDesiredOptionLocator).click();
        return this;
    }

    public PastebinPasteCreationResolutionScreen submitPaste() {
        WebElement submitPasteButton = webDriver.findElement(By.xpath("//*[@id=\"w0\"]/div[5]/div[1]/div[10]/button"));
        submitPasteButton.click();
        return new PastebinPasteCreationResolutionScreen(webDriver);
    }
}
