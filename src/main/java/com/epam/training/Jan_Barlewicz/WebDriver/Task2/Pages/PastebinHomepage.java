package com.epam.training.Jan_Barlewicz.WebDriver.Task2.Pages;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class PastebinHomepage {
    private final static String PASTEBIN_URL = "https://pastebin.com/";
    private final By postformTextLocator = By.id("postform-text");
    private final By postformNameLocator = By.id("postform-name");
    private final By postformExpirationTimeFieldLocator = By.xpath("//div[contains(@class,'postform-expiration')]//div[contains(@class,'col-sm-9 field-wrapper')]");
    private final By postformExpirationTimeOptionListLocator = By.id("select2-postform-expiration-results");
    private final By postformExpirationTimeDesiredOptionLocator = By.xpath("//li[contains(text(), \"10 Minutes\")]");


    private final By postformHighlightFormatFieldLocator = By.xpath("//div[contains(@class,'field-postform-format')]/descendant::span[contains(@class,'select2-selection__rendered')]");
    private final By postformHighlightFormatOptionListLocator = By.id("select2-postform-format-results");
    private final By postformHighlightFormatDesiredOptionLocator = By.xpath("//li[contains(text(), \"Bash\")]");

    private final By privacyPopupButtonLocator = By.xpath("//div[contains(@class,'qc-cmp2-summary-buttons')]/button[contains(@class,' css-47sehv')]");
    private final By submitPasteButtonLocator = By.xpath("//div[contains(@class,'form-group')]/button[contains(@type,'submit')]");
    private WebDriver webDriver;

    public PastebinHomepage(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public PastebinHomepage openPage(){
        webDriver.get(PASTEBIN_URL);
        return this;
    }

    public PastebinHomepage insertBashCode(){
        WebElement codeField = webDriver.findElement(postformTextLocator);
        codeField.sendKeys("git config --global user.name  \"New Sheriff in Town\"");
        codeField.sendKeys(Keys.ENTER);
        codeField.sendKeys("git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")");
        codeField.sendKeys(Keys.ENTER);
        codeField.sendKeys("git push origin master --force");
        return this;
    }

    public PastebinHomepage insertTile(String tileString){
        WebElement codeField = webDriver.findElement(postformNameLocator);
        codeField.sendKeys(tileString);
        return this;
    }

    public PastebinHomepage handlePrivacyPopup(){
        //new WebDriverWait(webDriver, Duration.ofSeconds(10))
        //        .until(ExpectedConditions.presenceOfElementLocated(privacyPopupButtonLocator));
        new WebDriverWait(webDriver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(privacyPopupButtonLocator));
        webDriver.findElement(privacyPopupButtonLocator).click();
        return this;
    }

    public PastebinHomepage setExpirationTo10M(){
        WebElement selectFieldDiv = webDriver.findElement(postformExpirationTimeFieldLocator);
        selectFieldDiv.click();
        WebElement selectedOptionUl = webDriver.findElement(postformExpirationTimeOptionListLocator);
        selectedOptionUl.findElement(postformExpirationTimeDesiredOptionLocator).click();
        return this;
    }

    public PastebinPasteCreationResult submitPaste(){
        WebElement submitPasteButton = webDriver.findElement(submitPasteButtonLocator);
        submitPasteButton.click();
        return new PastebinPasteCreationResult(webDriver);
    }

    public PastebinHomepage setHighlightToBash(){
        WebElement selectFieldDiv = webDriver.findElement(postformHighlightFormatFieldLocator);
        selectFieldDiv.click();
        WebElement selectedOptionUl = webDriver.findElement(postformHighlightFormatOptionListLocator);
        selectedOptionUl.findElement(postformHighlightFormatDesiredOptionLocator).click();
        return this;
    }
}
