package com.epam.training.Jan_Barlewicz.WebDriver.Task2.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Iterator;
import java.util.List;

public class PastebinPasteCreationResult {

    private WebDriver webDriver;
    private final By formattingStyleLocator = By.xpath("//div[contains(@class,'top-buttons')]/descendant::a");
    private final By pasteTextLocator = By.className("bash");
    PastebinPasteCreationResult(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public String getPasteText(){
        String pasteTextResult = "";
        //new WebDriverWait(webDriver, Duration.ofSeconds(10))
        //        .until(ExpectedConditions.presenceOfElementLocated(By.className("bash")));
        WebElement pasteText = webDriver.findElement(pasteTextLocator);
        List<WebElement> textLines = pasteText.findElements(By.className("li1"));
        Iterator <WebElement> textIterator = textLines.iterator();
        while(textIterator.hasNext()){
            WebElement textLine = textIterator.next();
            pasteTextResult += textLine.getText();
            if(textIterator.hasNext()){
                pasteTextResult+="\n";
            }
        }
        return pasteTextResult;
    }


    public String getPageTile(){
        return webDriver.getTitle();
    }

    public boolean isBashFormatting(){
        WebElement pasteText = webDriver.findElement(formattingStyleLocator);
        String formatting = pasteText.getText();
        return formatting.equals("Bash");
    }
}
