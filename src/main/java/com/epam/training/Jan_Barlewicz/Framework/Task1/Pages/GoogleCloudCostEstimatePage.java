package com.epam.training.Jan_Barlewicz.Framework.Task1.Pages;

import com.epam.training.Jan_Barlewicz.Framework.Task1.Model.CloudSpecification;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class GoogleCloudCostEstimatePage {
    private WebDriver webDriver;
    @FindBy(xpath = "//span[contains(text(),'Machine type')]/following-sibling::*")
    WebElement machineTypeResult;

    @FindBy(xpath = "//span[contains(text(),'GPU Model')]/following-sibling::*")
    WebElement gpuModelResult;
    @FindBy(xpath = "//span[contains(text(),'Local SSD')]/following-sibling::*")
    WebElement ssdTypeResult;
    @FindBy(xpath = "//span[contains(text(),'Number of Instances')]/following-sibling::*")
    WebElement noOfInstancesResult;
    @FindBy(xpath = "//span[contains(text(),'Operating System / Software')]/following-sibling::*")
    WebElement operatingSystemResult;
    @FindBy(xpath = "//span[contains(text(),'Provisioning Model')]/following-sibling::*")
    WebElement provisioningModelResult;
    @FindBy(xpath = "//span[contains(text(),'Region')]/following-sibling::*")
    WebElement regionResult;
    @FindBy(xpath = "//span[contains(text(),'Committed use discount options')]/following-sibling::*")
    WebElement commitedUseResult;


    GoogleCloudCostEstimatePage(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);

    }

    public boolean areValuesCorrect(CloudSpecification cloudSpecification){


        if(!isMachineTypeCorrect(cloudSpecification)){
            return false;
        }

        if(cloudSpecification.isAddGpu()) {
            if(!isGpuCorrect(cloudSpecification)){
                return false;
            }
        }

        if(!isSsdCorrect(cloudSpecification)){
            return false;
        }

        if(!isInstanceCorrect(cloudSpecification)){
            return false;
        }

        if(!isOpSystemCorrect(cloudSpecification)){
            return false;
        }

        if(!isProvModelCorrect(cloudSpecification)){
            return false;
        }

        if(!isRegionCorrect(cloudSpecification)){
            return false;
        }

        if(!isCommUseCorrect(cloudSpecification)){
            return false;
        }

        return true;
    }

    private boolean isMachineTypeCorrect(CloudSpecification cloudSpecification){
        if(!machineTypeResult.getText().contains(cloudSpecification.getMachineType())){
            return false;

        }
        return true;
    }
    private boolean isGpuCorrect(CloudSpecification cloudSpecification){
        if (!gpuModelResult.getText().equalsIgnoreCase(cloudSpecification.getGpuModel().replaceAll("-", " "))) {
            System.out.println(gpuModelResult.getText());
            System.out.println(cloudSpecification.getGpuModel());
            return  false;
        }
        return true;
    }
    private boolean isSsdCorrect(CloudSpecification cloudSpecification){
        if(!ssdTypeResult.getText().equals(cloudSpecification.getLocalSSD())){
            System.out.println(ssdTypeResult.getText());
            return false;
        }
        return true;
    }
    private boolean isInstanceCorrect(CloudSpecification cloudSpecification){
        if(!noOfInstancesResult.getText().equals(cloudSpecification.getNumberOfInstances().toString())){
            System.out.println(noOfInstancesResult.getText());
            return false;
        }
        return true;
    }
    private boolean isOpSystemCorrect(CloudSpecification cloudSpecification){
        if(!operatingSystemResult.getText().equals("Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)")){
            System.out.println(operatingSystemResult.getText());
            return false;
        }
        return true;
    }
    private boolean isProvModelCorrect(CloudSpecification cloudSpecification){
        if(!provisioningModelResult.getText().equals("Regular")){
            System.out.println(provisioningModelResult.getText());
            return false;
        }
        return true;
    }
    private boolean isRegionCorrect(CloudSpecification cloudSpecification){
        if(!regionResult.getText().equals(cloudSpecification.getRegion())){
            System.out.println(regionResult.getText());
            return false;
        }
        return true;
    }
    private boolean isCommUseCorrect(CloudSpecification cloudSpecification){
        if(!commitedUseResult.getText().equals(cloudSpecification.getCommitedUse()+" year")){
            System.out.println(commitedUseResult.getText());
            return false;
        }
        return true;
    }
}
