package com.epam.training.Jan_Barlewicz.Framework.Task1.Driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.BasicConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.time.Duration;

public class DriverManager {

    private static WebDriver webDriver;

    public static WebDriver getWebDriver() {
        if (webDriver == null) {
            switch (System.getProperty("browser")) {
                case  "firefox" : {
                    FirefoxOptions firefoxOptions = new FirefoxOptions();
                    firefoxOptions.addArguments("force-device-scale-factor=0.75");
                    firefoxOptions.addArguments("high-dpi-support=0.75");
                    WebDriverManager.firefoxdriver().setup();
                    webDriver = new FirefoxDriver();
                    break;
                }
                case  "edge" : {
                    EdgeOptions edgeOptions = new EdgeOptions();
                    edgeOptions.addArguments("force-device-scale-factor=0.75");
                    edgeOptions.addArguments("high-dpi-support=0.75");
                    WebDriverManager.edgedriver().setup();
                    webDriver = new EdgeDriver(edgeOptions);
                    break;
                }
                default:   {
                    ChromeOptions chromeOptionsOptions = new ChromeOptions();
                    chromeOptionsOptions.addArguments("force-device-scale-factor=0.75");
                    chromeOptionsOptions.addArguments("high-dpi-support=0.75");
                    WebDriverManager.chromedriver().setup();
                    webDriver = new ChromeDriver();
                    break;
                }
            }
            webDriver.manage().window().maximize();
            webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        }
        BasicConfigurator.configure();
        return webDriver;
    }

    public static void closeWebDriver() {
        webDriver.close();
        webDriver = null;
    }
}
