package com.epam.training.Jan_Barlewicz.Framework.Task1.Model;

public class CloudSpecification {
    Integer numberOfInstances;
    String machineType;
    boolean addGpg;
    String gpuModel;
    String localSSD;
    Integer commitedUse;
    String region;
    String expectedCost;

    public CloudSpecification(Integer numberOfInstances, String machineType, boolean addGpg, String gpuModel, String localSSD, Integer commitedUse, String region,String expectedCost) {
        this.numberOfInstances = numberOfInstances;
        this.machineType = machineType;
        this.addGpg = addGpg;
        this.gpuModel = gpuModel;
        this.localSSD = localSSD;
        this.commitedUse = commitedUse;
        this.region = region;
        this.expectedCost = expectedCost;
    }

    public Integer getNumberOfInstances() {
        return numberOfInstances;
    }

    public String getMachineType() {
        return machineType;
    }

    public boolean isAddGpu() {
        return addGpg;
    }

    public String getGpuModel() {
        return gpuModel;
    }

    public String getLocalSSD() {
        return localSSD;
    }

    public Integer getCommitedUse() {
        return commitedUse;
    }

    public String getRegion() {
        return region;
    }

    public void setNumberOfInstances(Integer numberOfInstances) {
        this.numberOfInstances = numberOfInstances;
    }

    public void setMachineType(String machineType) {
        this.machineType = machineType;
    }

    public void setAddGpg(boolean addGpg) {
        this.addGpg = addGpg;
    }

    public void setGpuModel(String gpuModel) {
        this.gpuModel = gpuModel;
    }

    public void setLocalSSD(String localSSD) {
        this.localSSD = localSSD;
    }

    public void setCommitedUse(Integer commitedUse) {
        this.commitedUse = commitedUse;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getExpectedCost() {
        return expectedCost;
    }

    public void setExpectedCost(String expectedCost) {
        this.expectedCost = expectedCost;
    }
}
