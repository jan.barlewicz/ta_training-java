package com.epam.training.Jan_Barlewicz.Framework.Task1.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class GoogleCloudSearchResults {
    private WebDriver webDriver;
    GoogleCloudSearchResults(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public GoogleCloudPriceCalculatorPage selectResult(){
        new WebDriverWait(webDriver, Duration.ofSeconds(10))
                .until(ExpectedConditions.presenceOfElementLocated(By.className("gsc-expansionArea")));
        WebElement searchResult = webDriver.findElement(By.className("gsc-expansionArea"));
        new WebDriverWait(webDriver, Duration.ofSeconds(10))
                .until(ExpectedConditions.presenceOfElementLocated(By.className("gs-webResult")));
        searchResult = searchResult.findElement(By.className("gs-webResult"));
        searchResult= searchResult.findElement(By.cssSelector("a"));
        String link = searchResult.getAttribute("href");
        webDriver.get(link);
        return new GoogleCloudPriceCalculatorPage(webDriver);
    }

    public int amountOfResults(){
        new WebDriverWait(webDriver, Duration.ofSeconds(10))
                .until(ExpectedConditions.presenceOfElementLocated(By.className("gsc-expansionArea")));
        WebElement searchResult = webDriver.findElement(By.className("gsc-expansionArea"));
        new WebDriverWait(webDriver, Duration.ofSeconds(10))
                .until(ExpectedConditions.presenceOfElementLocated(By.className("gs-webResult")));
        List<WebElement> listOfRes = searchResult.findElements(By.className("gs-webResult"));
        return  listOfRes.size();
    }


}
