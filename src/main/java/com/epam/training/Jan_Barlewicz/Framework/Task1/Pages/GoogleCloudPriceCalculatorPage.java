package com.epam.training.Jan_Barlewicz.Framework.Task1.Pages;

import com.epam.training.Jan_Barlewicz.Framework.Task1.Model.CloudSpecification;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class GoogleCloudPriceCalculatorPage {
    private WebDriver webDriver;

    @FindBy(xpath = "//div[contains(@class,'Gxwdcd')]/descendant::button")
    WebElement addToEstimateButton;
    @FindBy(xpath = "//div[contains(@class,'aHij0b-aGsRMb')]/child::div")
    WebElement computeEngineCostEstimateOption;
    @FindBy(id = "c11")
    WebElement numberOfInstancesInputField;
    @FindBy(xpath = "//div[contains(@class,'U4lDT')]/descendant::div[contains(@jsname,'kgDJk')]/descendant::div[contains(@class,'VfPpkd-aPP78e')]")
    WebElement machineTypeSelectionList;
    @FindBy(xpath = "//button[contains(@aria-label,'Add GPUs')]")
    WebElement addGpuSwitch;
    @FindBy(xpath = "//span[contains(text(), \"GPU Model\")]/ancestor::div[contains(@class, \"VfPpkd-TkwUic\")]/descendant::div[contains(@class, \"VfPpkd-aPP78e\")]")
    WebElement gpuModelSelectionList;
    @FindBy(xpath = "//span[contains(text(), \"Local SSD\")]/ancestor::div[contains(@class, \"VfPpkd-TkwUic\")]/descendant::div[contains(@class, \"VfPpkd-aPP78e\")]")
    WebElement localSSDSelectionList;
    @FindBy(xpath = "//span[contains(text(), \"Region\")]/ancestor::div[contains(@class, \"VfPpkd-TkwUic\")]/descendant::div[contains(@class, \"VfPpkd-aPP78e\")]")
    WebElement regionSelectionList;


    public GoogleCloudPriceCalculatorPage(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);

    }

    public GoogleCloudPriceCalculatorPage goToEstimate() {
        addToEstimateButton.click();
        computeEngineCostEstimateOption.click();

        return this;
    }

    public GoogleCloudPriceCalculatorPage setValues(CloudSpecification cloudSpecification) {

        setInstances(cloudSpecification);

        new WebDriverWait(webDriver, Duration.ofSeconds(10))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"glue-cookie-notification-bar-1\"]/button")));
        webDriver.findElement(By.xpath("//*[@id=\"glue-cookie-notification-bar-1\"]/button")).click();

        setMachineType(cloudSpecification);

        if (cloudSpecification.isAddGpu()) {
            setGpu(cloudSpecification);
        }

        setSsd(cloudSpecification).
                setCommitedUse(cloudSpecification).
                setRegion(cloudSpecification);

        return this;
    }

    private GoogleCloudPriceCalculatorPage setInstances(CloudSpecification cloudSpecification){
        numberOfInstancesInputField.clear();
        numberOfInstancesInputField.sendKeys(cloudSpecification.getNumberOfInstances().toString());
        return this;
    }
    private GoogleCloudPriceCalculatorPage setMachineType(CloudSpecification cloudSpecification){
        machineTypeSelectionList.click();
        webDriver.findElement(By.xpath("//ul[contains(@aria-label, \"Machine type\")]//descendant::li[contains(@data-value, \"" + cloudSpecification.getMachineType() + "\")]")).click();
        return this;
    }
    private GoogleCloudPriceCalculatorPage setGpu(CloudSpecification cloudSpecification){
        addGpuSwitch.click();
        gpuModelSelectionList.click();
        webDriver.findElement(By.xpath("//ul[contains(@aria-label, \"GPU Model\")]/li[contains(@data-value, \"" + cloudSpecification.getGpuModel() + "\")]")).click();
        return this;
    }
    private GoogleCloudPriceCalculatorPage setSsd(CloudSpecification cloudSpecification){
        localSSDSelectionList.click();
        webDriver.findElement(By.xpath("//span[contains(text(), '" + cloudSpecification.getLocalSSD() + "')]/ancestor::li")).click();
        return this;
    }
    private GoogleCloudPriceCalculatorPage setCommitedUse(CloudSpecification cloudSpecification){
        webDriver.findElement(By.xpath("//label[contains(@for,'" + cloudSpecification.getCommitedUse() + "-year')]/parent::div")).click();
        return this;
    }
    private GoogleCloudPriceCalculatorPage setRegion(CloudSpecification cloudSpecification){
        regionSelectionList.click();
        webDriver.findElement(By.xpath("//span[contains(text(), '" + cloudSpecification.getRegion() + "')]/ancestor::li")).click();
        return this;
    }


    public GoogleCloudPriceCalculatorPage shareEstimate(CloudSpecification cloudSpecification) {

        new WebDriverWait(webDriver, Duration.ofSeconds(10))
                .until(ExpectedConditions.textToBe(By.xpath("//*[@id=\"ucj-1\"]/div/div/div/div/div/div/div/div[1]/div/div[1]/div[3]/span[1]"), cloudSpecification.getExpectedCost()));
        webDriver.findElement(By.xpath("//button[contains(@aria-label,'Open Share Estimate dialog')]")).click();
        return this;
    }

    public String getPriceEstimate() {
        String priceEstimate = webDriver.findElement(By.xpath("//*[@id=\"yDmH0d\"]/div[5]/div[2]/div/div/div/div[2]/div[1]/div/div/label")).getText();
        return priceEstimate;
    }

    public GoogleCloudCostEstimatePage goToSummary() {
        WebElement sumField = webDriver.findElement(By.xpath("//div[contains(@class,'ZgevAb')]"));
        String link = sumField.findElement(By.cssSelector("a")).getAttribute("href");
        webDriver.get(link);
        return new GoogleCloudCostEstimatePage(webDriver);
    }

}
