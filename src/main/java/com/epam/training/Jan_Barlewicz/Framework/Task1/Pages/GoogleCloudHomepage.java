package com.epam.training.Jan_Barlewicz.Framework.Task1.Pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
public class GoogleCloudHomepage {
    private final String webAdress = "https://cloud.google.com/";
    private WebDriver webDriver;

    @FindBy(xpath = "//input[contains(@class,'mb2a7b')]")
    WebElement searchBar;


    public GoogleCloudHomepage(WebDriver webDriver) {
        this.webDriver = webDriver;
        webDriver.get(webAdress);
        PageFactory.initElements(webDriver, this);
    }

    public GoogleCloudSearchResults performSearchForItems(String searchTerm) {
        searchBar.click();
        searchBar.sendKeys(searchTerm);
        searchBar.sendKeys(Keys.ENTER);
        return new GoogleCloudSearchResults(webDriver);
    }
}
