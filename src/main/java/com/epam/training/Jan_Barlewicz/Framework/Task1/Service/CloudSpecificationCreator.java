package com.epam.training.Jan_Barlewicz.Framework.Task1.Service;

import com.epam.training.Jan_Barlewicz.Framework.Task1.Model.CloudSpecification;

public class CloudSpecificationCreator {

    public static final String numberOfInstances = "testdata.spec.instances";
    public static final String machineType = "testdata.spec.machineType";
    public static final String addGpg = "testdata.spec.addGpu";
    public static final String gpuModel = "testdata.spec.gpuModel";
    public static final String localSSD = "testdata.spec.localSSD";
    public static final String commitedUseYears = "testdata.spec.commitedUseYears";
    public static final String region = "testdata.spec.region";
    public static final String expectedCost = "testdata.spec.expectedCost";
    public static CloudSpecification withDefaultSettings(){
        return new CloudSpecification(Integer.valueOf(DataReader.getTestDataString(numberOfInstances)),
                DataReader.getTestDataString(machineType),
                Boolean.valueOf(DataReader.getTestDataString(addGpg)),
                DataReader.getTestDataString(gpuModel),
                DataReader.getTestDataString(localSSD),
                Integer.valueOf(DataReader.getTestDataString(commitedUseYears)),
                DataReader.getTestDataString(region),
                DataReader.getTestDataString(expectedCost));
    }
}
