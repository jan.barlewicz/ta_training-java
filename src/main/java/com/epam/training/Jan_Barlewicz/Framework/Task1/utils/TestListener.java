package com.epam.training.Jan_Barlewicz.Framework.Task1.utils;

import com.epam.training.Jan_Barlewicz.Framework.Task1.Driver.DriverManager;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;


public class TestListener implements ITestListener {
    private Logger log = LogManager.getRootLogger();
    @Override
    public void onTestFailure(ITestResult result) {
        saveScreenshot();
    }


    private void saveScreenshot() {
    File screenshot = ((TakesScreenshot) DriverManager.getWebDriver()).getScreenshotAs(OutputType.FILE);
    try{
        FileUtils.copyFile(screenshot,new File(".//target/screenshots/"+getCurrentTimeAsString()+".png"));
    }catch (IOException e){
        log.error("Failed to save screenshoot: "+e.getLocalizedMessage());
    }


}

    private String getCurrentTimeAsString() {
        DateTimeFormatter formatter= DateTimeFormatter.ofPattern("uuuu-MM-dd-HH-mm-ss");
        return ZonedDateTime.now().format(formatter);
    }
    }
