package com.epam.training.Jan_Barlewicz.Framework.Task1.Service;

import java.util.ResourceBundle;

public class DataReader {
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle(System.getProperty("environment"));

    public static String getTestDataString(String key) {
        return resourceBundle.getString(key);


    }
}
